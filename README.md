<h1> Projeto 1 - Programação para Web </h1> 
<p>  HTML e CSS: Este projeto consiste da elaboração de uma página web de conteúdo estatico, utilizando apenas as tecnologias HTML 
e CSS para construção de um layout de um website. </p>
<p> Foi desenvolvido por Luiz Miguel Naldi Dias e Luan Antônio Dias. A reprodução do site escolhido foi o <a href="https://www.netflix.com/br/">Netflix</a>.</p> 
<p> O resultado final pode ser observado neste <a href="https://miguelnaldi.github.io/programacao-web/">link</a>. </p>
